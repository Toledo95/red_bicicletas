var map = L.map('main_map').setView([-25.3524678, -57.5245382], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9sZWRvcHkiLCJhIjoiY2tpMjNkaXFpMWpkZTJxbm10d3VpOXN6ayJ9.tY0w370I4xyEeMGZcExkVA', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
}).addTo(map);



$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})