var Bicicleta = require('../../models/bicicleta');

beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis',()=>{
    it('comienza vacio', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicletas.add', () => {
    it('Agregamos una bicicleta',() => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-25.3524678, -57.5245382]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis[0]).toBe(a);
    });
} );


describe('Bicicletas.findById', () => {
    it('Debe devolver la bici con id 1',() => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici1 = new Bicicleta(1, 'Azul', 'urbana', [-25.3524673, -57.5245373]);
        Bicicleta.add(aBici1);
        var aBici2 = new Bicicleta(2, 'Verde', 'Pistera', [-25.3524663, -57.5245363]);
        Bicicleta.add(aBici2);


        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);
    });
} );


describe('Bicicletas.removeById', () => {
    it('Debe eliminar la bici con id 1',() => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici1 = new Bicicleta(1, 'Azul', 'urbana', [-25.3524673, -57.5245373]);
        Bicicleta.add(aBici1);
        var aBici2 = new Bicicleta(2, 'Verde', 'Pistera', [-25.3524663, -57.5245363]);
        Bicicleta.add(aBici2);

        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
    });
} );


