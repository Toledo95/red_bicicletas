var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () =>{
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            request.get('http://localhost:5000/api/bicicletas', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                
            });
        });

    });

    describe('POST BICICLETAS /create', () =>{
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';

            request.post({
                headers : headers,
                url : 'http://localhost:5000/api/bicicletas/create',
                body : aBici
            }, function(error, response, body)  {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });

    });

    describe('POST BICICLETAS /create', () =>{
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';

            request.post({
                headers : headers,
                url : 'http://localhost:5000/api/bicicletas/create',
                body : aBici
            }, function(error, response, body)  {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });

    });

    describe('POST BICICLETAS /:id/update', () =>{
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "azul", "modelo": "pista", "lat": -36, "lng": -85 }';

            request.post({
                headers : headers,
                url : `http://localhost:5000/api/bicicletas/10/update`,
                body : aBici
            }, function(error, response, body)  {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("azul");
                expect(Bicicleta.findById(10).modelo).toBe("pista");
                expect(Bicicleta.findById(10).ubicacion[0]).toBe(-36);
                expect(Bicicleta.findById(10).ubicacion[1]).toBe(-85);
                done();
            });
        });

    });

    describe('DELETE BICICLETAS /:id/delete', () =>{
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
           

            request.delete({
                headers : headers,
                url : `http://localhost:5000/api/bicicletas/10/delete`
            }, function(error, response, body)  {
                expect(response.statusCode).toBe(204);
                done();
            });
        });

    });


});
